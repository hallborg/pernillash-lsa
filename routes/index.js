var express = require('express');
var router = express.Router();
var db = require('../utils/mysql');

String.prototype.format = function() {
  var formatted = this;
  for (var i = 0; i < arguments.length; i++) {
    var regexp = new RegExp('\\{'+i+'\\}', 'gi');
    formatted = formatted.replace(regexp, arguments[i]);
  }

  return formatted;
};

function addVisit(ip) {
  var d = new Date();
  d = d.toISOString().substring(0, 19).replace('T', ' ');
  var query = 'INSERT INTO visits (location, visitor, date) VALUES ("{0}", "{1}", "{2}")';
  console.log(query.format('pernillashalsa', ip, d));
  db.query(query.format('pernillashalsa', ip, d), function(err, result, fields) {
    if (err != null) {
      console.log('sql insert err :' + err);
    }
  });
}

function addUniqueIp(ip) {
  var query = 'INSERT INTO unique_ips (ipv4) VALUES ("{0}")';
  db.query(query.format(ip), function(err, result, fiedls) {
    if (err != null) {
      console.log('sql insert err :' + err);
    }
  });
}

function checkIp(ip) {
  var query = 'SELECT ipv4 FROM unique_ips WHERE ipv4 = "{0}"';
  db.query(query.format(ip), function(err, result, fields) {
    if (err == null) {
      if (result.length > 0) {
        console.log('old ip');
        // ip exists in the database
        // just add it to visits
        addVisit(ip);
      }
      else {
        console.log('new ip');
        // new ip! add it to unique_ips
        // and add it to visits
        addUniqueIp(ip);
        addVisit(ip);
      }
    }
    else {
      console.log('error' + err);
    }
  });
}
/* GET home page. */
router.get('/', function(req, res, next) {
  var ip = req.headers['x-real-ip'];
  if (ip == undefined) ip = '127.0.0.1';
  checkIp(ip);
  	res.render('index', {
  		title: 'Start',
  		selected: 0
  	});
});

router.get('/betraktelser', function (req, res, next) {
	res.render('betraktelser', {
		title: 'Betraktelser',
		selected: 1
	});
});

router.get('/boktips', function (req, res, next) {
	res.render('boktips', {
		title: 'boktips',
		selected: 2
	});
});

router.get('/kost', function (req, res, next) {
	res.render('kost', {
		title: 'kost',
		selected: 3
	});
});

router.get('/recept', function (req, res, next) {
	res.render('recept', {
		title: 'Recept',
		selected: 4
	});
});

router.get('/om', function (req, res, next) {
	res.render('om', {
		title: 'Om',
		selected: 5
	});
});

router.get('/kontakt', function (req, res, next) {
	res.render('kontakt', {
		title: 'Kontakt',
		selected: 8
	});
});

router.get('/ayurveda', function(req, res, next) {
    res.render('ayurveda', {
        title: 'Ayurveda',
        selected: 6
    });
});
router.get('/strykningsterapi', function(req, res, next) {
    res.render('strykningsterapi', {
        title: 'Strykningsterapi',
        selected: 7
    })
})

module.exports = router;
